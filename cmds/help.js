const menus = {
    main: `
      chow [command] <options>
  
      log ................. log some food
      list ................ list some food`,
  
    log: `
      chow log <options>
  
      --name, -n .................. name of food
      --date, -d .................. date (mm.dd.yyyy) of consumption 
      --calories, -c .............. calories of food`,
  }
  
  module.exports = (args) => {
    const subCmd = args._[0] === 'help'
      ? args._[1]
      : args._[0]
  
    console.log(menus[subCmd] || menus.main)
  }