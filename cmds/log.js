module.exports = (args) => {

    let cmd = args._[0]

    if (args.name || args.n) {
        cmd = "name"
    }

    if (args.date || args.d) {
        cmd = "date"
    }

    if (args.calories || args.c) {
        cmd = "calories"
    }

    switch (cmd) {
        case 'name':
            console.log(args)
            break
        case 'date':
            console.log(args)
            break
        case 'calories':
            console.log(args)
            break

      default:
        console.log("log requires arguments. Try <chow log --help> for help.")
        break
    }
}