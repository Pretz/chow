const minimist = require('minimist')

module.exports = () => {
    // Trims first two inputs of an argument
    const args = minimist(process.argv.slice(2))
    // Displays help screen if no arguments are passed
    let cmd = args._[0] || 'help'

    if (args.version || args.v) {
        cmd = 'version'
    }

    if (args.help || args.h) {
        cmd = 'help'
    }
  
    switch (cmd) {
        case 'log':
            require('./cmds/log')(args)
            break
        case 'version':
            require('./cmds/version')(args)
            break
        case 'help':
            require('./cmds/help')(args)
            break

      default:
        console.error(`"${cmd}" is not a valid command!`)
        break
    }
  }